const path = require('path');

module.exports = {
  mode: 'development',
  entry: {index: './src/index.js',
          service: './src/service.js'},
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: 'dist/',
    chunkFilename: '[name].bundle.js',
    libraryTarget: 'umd',
    globalObject: 'this',
    library: '[name]'
  },
   module: {
     rules: [
       {
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader',
         ],
       },
     ],
   },
   watch: true
};