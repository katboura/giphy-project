import axios from 'axios';
import * as controller from './index.js';

const BASE_URL = 'http://api.giphy.com/v1/gifs';
const API_KEY = 'XZ0Ah7Xw0VtjTfhVEHzX9I9KboDx8ZSJ';
let imgSrcs = [];

//call GIPHY service to get relevant gifs
export function getGifs (searchQuery) {
    axios.get(BASE_URL+'/search?q='+searchQuery+'&api_key='+API_KEY)
    .then(function (response) {
      const gifs = response.data.data;
        imgSrcs = _.map(gifs, function(item){
        return item.images.fixed_height.url;
      });
      controller.addGifsToDOM( imgSrcs );
      persistResults( imgSrcs )
    })
    .catch(function (error) {
      // show error to user
      alert('Ooops something went wrong: '+error);
    })
};

//called when user deletes a GIF and handles the results array - removes items
export function removeItemFromList( src ){
  const indexFound = _.findIndex(imgSrcs, function(item) {
    return item === src;
  })

  if (indexFound !== -1){
    imgSrcs.splice(indexFound, 1);
    persistResults(imgSrcs);
  }
}

//retrieves results from local storage
export function getPersistedResults( ) {
  //create object to save in local storage 
  let stored = {};
  if (localStorage.getItem('persistedSearch')){
    stored = JSON.parse(localStorage.getItem('persistedSearch'));
  } else {
    stored.term = '';
    stored.results = [];
  }
  
  imgSrcs = stored.results;
  return stored;
}

//saves the results in local storage
export function persistResults( results ) {
  
  //create object to save in local storage 
  const search = {};

  search.term = document.getElementById("gifsearch").value ;
  search.results = results;
  localStorage.setItem('persistedSearch', JSON.stringify(search))
}

