import _ from 'lodash';
import './style.css';
import * as service from './service.js';
import 'bootstrap/dist/css/bootstrap.min.css';

//show relative message when no gifs to show
function showNoImagesMsg(){
  resultsDiv.innerHTML = 'No gifs to show! Try something else...';
}

//delete element function
function deleteElement( src ){
  const imgToDelete = document.getElementById(src);
  const parentDivtoDelete = imgToDelete.parentNode;
  const saved = service.getPersistedResults();

  //delete from UI and persist results
  parentDivtoDelete.parentElement.removeChild(parentDivtoDelete);
  service.removeItemFromList( src );  
  
  if (saved.results.length === 0) {
    showNoImagesMsg();
  }
}

//copy image url to clipboard function
function copyToClipboard ( url ){
  const temp = document.createElement('textarea');

  //create temporary element to perform the copy
  temp.value = url;
  document.body.appendChild(temp);
  temp.select();
  document.execCommand('copy');
  document.body.removeChild(temp);
}

//create elements in order to show gifs
function createImages (src) {
  const divForImage = document.createElement('div');
  const img = document.createElement('img');
  const buttonCopyLink = document.createElement('div');  
  const buttonDelete = document.createElement('div');   
  const gridContainerImgs = document.createElement('div');

  //create grid layout only for the buttons on the gifs
  gridContainerImgs.classList.add('grid-container');  
  divForImage.classList.add('imgDiv');
  
  //create copy link btn and add it as grid item
  buttonCopyLink.innerHTML = "Copy link";
  buttonCopyLink.classList.add('copyLinkBtn');
  buttonCopyLink.classList.add('grid-item');
  buttonCopyLink.addEventListener('click', function(){
    copyToClipboard(src);
  });
  
  //create delete btn and add it as grid item
  buttonDelete.innerHTML = "Delete";
  buttonDelete.classList.add('deleteLinkBtn');
  buttonDelete.classList.add('grid-item');
  buttonDelete.addEventListener('click', function(){
    deleteElement(src);
  });

  //create the GIF
  img.src = src;
  img.id = src;
  img.classList.add("imgFixed");
  
  divForImage.appendChild(img);
  divForImage.appendChild(gridContainerImgs);
  gridContainerImgs.appendChild(buttonCopyLink);
  gridContainerImgs.appendChild(buttonDelete);
  return divForImage;
};

//add gifs from service to HTML DOM
export function addGifsToDOM (gifs) {
  const resultsDiv = document.getElementById("resultsDiv");

  if (Array.isArray(gifs) && gifs.length > 0) {
    //first clear container element
    resultsDiv.innerHTML = '';

    //then fill it with the new gifs
    _.map(gifs, function(gif){
      if(resultsDiv){
        resultsDiv.appendChild(createImages(gif));
      } 
    })
  } else {
    showNoImagesMsg();
  }
};

//onclick event upon Search button pressed by user
export function searchGifs() { 
  const query = document.getElementById("gifsearch").value;
  service.getGifs(query);
};

//trigger search Gifs function when the user presses Enter after typing
export function enterKeySearch(event){
  if (event.keyCode === 13) {
    searchGifs();
  }
}

//onclick event upon Clear All button pressed by user
export function clearAll() { 
  const resultsDiv = document.getElementById("resultsDiv");
  const toPersistObj = {};

  //update UI
  resultsDiv.innerHTML = '';
  document.getElementById("gifsearch").value = '';
  showNoImagesMsg();  
  
  //persist empty array and no term
  toPersistObj.term = '';
  toPersistObj.results = [];
  service.persistResults(toPersistObj);
};

//fill up the page with the previously stored results and search term
export function fillUpWithPreviousResults(){
  const saved = service.getPersistedResults();
  document.getElementById("gifsearch").value = saved.term;
  addGifsToDOM(saved.results);
}