
# Giphy demo

This is a simple web application created with javascript which uses GIPHY API in order to show GIFs according to a search term and manage them.  

## Features

These are the features provided to the user:

- Search GIF with a term (Button Search or press Enter after typing).
- Clear all (Clear all button to delete search term and GIF results).
- Delete GIF from the list (Delete button provided on every GIF).
- Copy link of GIF to clipboard (Copy link button provided on every GIF).
- Persist results in Web Storage after any action (getting GIFs or after deleting). This will help the user to have the results loaded on the screen that had before refreshing the page.

## Installation

1. This project requires [Node.js](https://nodejs.org/) v12.14 or higher to run. Download and install it.
2. Run command prompt as administrator and go to the project folder (```cd giphy-project-master```).
3. Hit ```npm install``` in order to install all the project dependencies.
4. Go to the project folder and enter file giphy-project-master/dist.
5. Double-click file index.html. This should open a browser page which is running the demo.

## Libraries used

This demo uses a number of open source projects to work properly:

- [Webpack](https://webpack.js.org/) :  Static module bundler for modern JavaScript applications. It is used to internally build a dependency graph, which maps every module a project needs and generates in one or more bundles.
- [GIPHY API](https://developers.giphy.com/) : Integrate the world's largest GIF library to any project.
- [Axios](https://github.com/axios/axios) : Promise based HTTP client for the browser and node.js.
- [Bootstrap](https://getbootstrap.com/) : Toolkit for responsive web apps. Upon istallation also libraries JQuery and popper.js were istalled but were not used in the rest of the implementation.
- [Lodash](https://lodash.com/) : Utility library delivering modularity and performance by providing functions to work with arrays, numbers, objects, strings, etc.

## Development

Upon development, after editing the project, webpack needs to rebuild project after every change.
A build script has been created in package.json file in order to do this easily.
Webpack is set, in this project, to be watching for changes (```watch: true``` configuration in webpack.config.js) so it is enough to run the build script only one time and then just refresh the browser page after any change in the code, as webpack is following the changes. To start webpack watch and build the project hit ```npm run build``` into the project folder.
